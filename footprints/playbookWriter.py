# -*- coding: utf-8 -*-
from __future__ import (absolute_import, division, print_function)

import json
from collections import namedtuple
import subprocess
import boto3
import yaml
import pyaml

import os
import sys


import ansible
from ansible.parsing.dataloader import DataLoader
from ansible.vars import VariableManager
from ansible.inventory import Inventory
from ansible.inventory import group
from ansible.playbook.play import Play
from ansible.executor.task_queue_manager import TaskQueueManager
from ansible.plugins.callback.json import CallbackModule
from ansible.plugins.callback import CallbackBase
from factsParsing import *


# Playbook yaml generation from a dict -------------------------------------------

def writePlaybookFile2(myplaybookdict):

    """
        myplaybook is a list of dicts, where each dict is
        an action of the playbook
    """

    output = yaml.dump(myplaybookdict, default_flow_style=True, explicit_start=True)

    myplaybook = open('myplaybook.yml','w')

    myplaybook.write(output)
    myplaybook.close()

def writePlaybookFile(myplaybookdict):

    """
        myplaybook is a list of dicts, where each dict is
        an action of the playbook
    """

    myplaybook = open('myplaybook.yml','w')
    myplaybook.write("---" + os.linesep)

    for task in myplaybookdict:
        myplaybook.write("- name: " + task["name"] + os.linesep)
        myplaybook.write("  " + task["action"]["module"]+":"+ os.linesep)
        for arg, value in task["action"]["args"].items():
            if isinstance(value, basestring):
                myplaybook.write("    " + arg + ": " + value + os.linesep )
            elif isinstance(value, dict):
                myplaybook.write("    " + arg + ": "+ os.linesep )
                for k,v in value.items():
                    if isinstance(v, basestring):
                        myplaybook.write("      - " + k + ": " + v + os.linesep )
            elif isinstance(value, list):
                myplaybook.write("    " + arg + ": " + os.linesep )
                for v in value:
                    if isinstance(v, basestring):
                        myplaybook.write("      - " + v + os.linesep )


        myplaybook.write(""+ os.linesep )
        # handle recursively lists and dicts
        # register

    myplaybook.close()


