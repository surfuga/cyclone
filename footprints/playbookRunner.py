# -*- coding: utf-8 -*-
from __future__ import (absolute_import, division, print_function)

import json
from collections import namedtuple
import subprocess
import boto3
import yaml
import pyaml
import pprint
import os
import sys


import ansible
from ansible.parsing.dataloader import DataLoader
from ansible.vars import VariableManager
from ansible.inventory import Inventory
from ansible.inventory import group
from ansible.playbook.play import Play
from ansible.executor.task_queue_manager import TaskQueueManager
from ansible.plugins.callback.json import CallbackModule
from ansible.plugins.callback import CallbackBase



def provisionPlaybook(myplaybook):

  Options = namedtuple('Options', ['connection', 'module_path', 'forks', 'become', 'become_method', 'become_user', 'check'])
  # initialize needed objects
  variable_manager = VariableManager()
  loader = DataLoader()
  options = Options(connection='local', module_path='/path/to/mymodules', forks=100, become=None, become_method=None, become_user=None, check=False)
  passwords = dict(vault_pass='secret')

  # Instantiate our ResultCallback for handling results as they come in
  #results_callback = ResultCallback()
  # Now use json callback to get an object
  json_callback = CallbackModule()

  # Inventory
  # create inventory and pass to var manager
  inventory = Inventory(loader=loader, variable_manager=variable_manager, host_list='./ec2.py')
  # print(dir(inventory))
  # print(inventory.get_hosts())
  # that print shows that ec2.py is working,
  # the problem is that ec2/AWS ansible modules work from
  # hosts: localhost
  # connection: local


  # finally set inventory
  variable_manager.set_inventory(inventory)

  # create play with tasks
  play_source =  dict(
          name = "Ansible Play",
          connection = 'local',
          hosts = 'localhost',
          gather_facts = 'False',
          tasks = myplaybook
      )
  play = Play().load(play_source, variable_manager=variable_manager, loader=loader)
  # actually run it
  tqm = None
  try:
      tqm = TaskQueueManager(
                inventory=inventory,
                variable_manager=variable_manager,
                loader=loader,
                options=options,
                passwords=passwords,
                stdout_callback=json_callback,  # Use our custom callback instead of the ``default`` callback plugin
            )
      result = tqm.run(play)

      #print("finished play")
      #print(json_callback.results[0]['tasks'])

  finally:
      if tqm is not None:
          tqm.cleanup()

  return json_callback


class ResultCallback(CallbackBase):
    """A sample callback plugin used for performing an action as results come in

    If you want to collect all results into a single object for processing at
    the end of the execution, look into utilizing the ``json`` callback plugin
    or writing your own custom callback plugin
    """
    def v2_runner_on_ok(self, result, **kwargs):
        """Print a json representation of the result

        This method could store the result in an instance attribute for retrieval later
        """
        host = result._host
        print (json.dumps({host.name: result._result}, indent=4))
