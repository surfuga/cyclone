# -*- coding: utf-8 -*-
from __future__ import (absolute_import, division, print_function)

import json
from collections import namedtuple
import subprocess
import boto3
import yaml
import pyaml

import os
import sys
from collections import OrderedDict


import ansible
from ansible.parsing.dataloader import DataLoader
from ansible.vars import VariableManager
from ansible.inventory import Inventory
from ansible.inventory import group
from ansible.playbook.play import Play
from ansible.executor.task_queue_manager import TaskQueueManager
from ansible.plugins.callback.json import CallbackModule
from ansible.plugins.callback import CallbackBase
from factsParsing import *


# Playbook generation as a dict -------------------------------------------

def vpcPlaybook(infrastructure, region, translation):

  tasks = []

  # vpc ----------------------------------------------------

  i = 0

  for vpc in infrastructure['vpcs']:

    # filtering phase (exclude identifiers from a list ) PENDING
    # if vpc[u'tags'][u'Name'] == u"VPC-VINT-20160713-1232":
    #   continue

    # current var
    identifier = u'vpc' + u"%d" % i

    # save current vpc id to translation table with register value
    translation[vpc['id']]='{{ ' +  identifier + '.vpc.id'  + ' }}'

    # get tags
    mytags = dict()
    for tagname, tagvalue in vpc['tags'].items():
      auxdict = {}
      if tagname != u"Name" and not tagname.startswith("aws"):
        auxdict[tagname]=tagvalue
        mytags.update(auxdict)

    mytags['cloned'] = 'yes'

    action = [
      OrderedDict(name="VPC | create vpc",
        action=OrderedDict(
          module='ec2_vpc_net',
          args=dict(
            region=region,
            name=vpc[u'tags'][u'Name'],
            cidr_block=vpc[u'cidr_block'],
            multi_ok='True',
            tags=mytags,
            )
          ),
          register= identifier
        ),
    ]

    i += 1

    tasks.extend(action)

  # print("translation table after vpcs")
  # pp = pprint.PrettyPrinter(indent=4)
  # pp.pprint(translation)

  return tasks


def igwPlaybook(infrastructure, region, translation):

  tasks = []

  i = 0

  for igw in infrastructure['igws']:

    # filtering phase (exclude identifiers from a list ) PENDING

    # current var
    identifier = u'igw' + u"%d" % i
    # save current igw id to translation table with register value
    translation[igw['internet_gateway_id']]='{{ ' + identifier + '.gateway_id'  + ' }}'

    # translate any needed id to translation table value
    # --> how to use jinja vars in
    for attachment in igw["attachments"]:
      #print(" before translation: ",attachment["vpc_id"])
      attachment["vpc_id"]=translation[attachment["vpc_id"]]
      #print(" after translation: ",attachment["vpc_id"])

    # get tags
    mytags = dict()
    for tagdict in igw['tags']:
      for tagname, tagvalue in tagdict.items():
        auxdict = {}
        if tagname != u"Name" and not tagname.startswith("aws"):
          auxdict[tagname]=tagvalue
          mytags.update(auxdict)

    mytags['cloned'] = 'yes'

    action = [
      OrderedDict(name="IGW | create igw",
        action=OrderedDict(
          module='ec2_vpc_igw',
          args=dict(
            region=region,
            vpc_id=igw['attachments'][0]['vpc_id'],
            state='present',
            #tags=mytags,
            )
          ),
          register=identifier
        ),
    ]

    i += 1

    tasks.extend(action)

  # print("translation table after igws")
  # pp = pprint.PrettyPrinter(indent=4)
  # pp.pprint(translation)

  return tasks

def subnetPlaybook(infrastructure, region, translation):

  tasks = []

  i = 0

  for subnet in infrastructure['subnets']:

    # filtering phase (exclude identifiers from a list ) PENDING

    # current var
    identifier = u'subnet' + u"%d" % i
    # save current subnet id to translation table with register value
    translation[subnet['id']]='{{ ' + identifier + '.subnet.id'  + ' }}'

    # translate any needed id to translation table value
    # --> how to use jinja vars in
    #print(" before translation: ",subnet["vpc_id"])
    subnet["vpc_id"]=translation[subnet["vpc_id"]]
    #print(" after translation: ",subnet["vpc_id"])

    # get tags
    mytags = dict()
    for tagname, tagvalue in subnet['tags'].items():
        auxdict = {}
        if not tagname.startswith("aws"):
          auxdict[tagname]=tagvalue
          mytags.update(auxdict)

    mytags['cloned'] = 'yes'

    action = [
      OrderedDict(name="Subnets | create subnet",
        action=OrderedDict(
          module='ec2_vpc_subnet',
          args=dict(
            cidr=subnet["cidr_block"],
            region=region,
            vpc_id=subnet["vpc_id"],
            state='present',
            tags=mytags,
            #map_public_ip_on_launch # not yet implemented
            )
          ),
          register=identifier
        ),

    ]

    i += 1

    tasks.extend(action)

  # print("translation table after subnets")
  # pp = pprint.PrettyPrinter(indent=4)
  # pp.pprint(translation)

  return tasks

def naclPlaybook(infrastructure, region, translation):

  tasks = []

  i = 0

  for nacl in infrastructure['nacls']:

    # filtering phase (exclude identifiers from a list ) PENDING

    # current var
    identifier = u'nacl' + u"%d" % i
    # save current subnet id to translation table with register value
    translation[nacl['nacl_id']]='{{ ' + identifier + '.nacl.id'  + ' }}'

    # translate any needed id to translation table value
    # --> how to use jinja vars in
    #print(" before translation: ",nacl["vpc_id"])
    nacl["vpc_id"]=translation[nacl["vpc_id"]]
    #print(" after translation: ",nacl["vpc_id"])

    #subnets
    for j in range(len(nacl["subnets"])):
      subnet = nacl["subnets"][j]
      #print(" before translation: ",subnet)
      subnet2=translation[subnet]
      #print(" after translation: ",subnet2)
      nacl['subnets'][j]=subnet2

    #ingress -> no need to change it

    #egress -> no need to change it

    # get tags
    mytags = dict()
    for tagname, tagvalue in nacl['tags'].items():
        auxdict = {}
        if not tagname.startswith("aws"):
          auxdict[tagname]=tagvalue
          mytags.update(auxdict)

    mytags['cloned'] = 'yes'

    action = [
      OrderedDict(name="Nacls | create nacl",
        action=OrderedDict(
          module='ec2_vpc_nacl',
          args=dict(
            vpc_id=nacl["vpc_id"],
            name=identifier,
            region=region,
            state='present',
            subnets=nacl['subnets'],
            ingress=nacl['ingress'],
            egress=nacl['egress'],
            tags=mytags,
            )
          ),
          ignore_errors=True,
          register=identifier
        ),
    ]

    i += 1

    tasks.extend(action)

  # print("translation table after nacls")
  # pp = pprint.PrettyPrinter(indent=4)
  # pp.pprint(translation)
  return tasks


def prepareSgList(sglist):
  return [ x['group_id'] for x in sglist ]


def addIpv4permissions(ip_permission, new_ip_permissions):
  for k in range(len(ip_permission["ip_ranges"])):
    # copy all attributes to new ip_permission
    newipp = {}
    newipp["proto"]=ip_permission['proto']
    if "from_port" in ip_permission:
      newipp["from_port"]=ip_permission["from_port"]
    if "to_port" in ip_permission:
      newipp["to_port"]=ip_permission["to_port"]
    newipp["cidr_ip"]=ip_permission["ip_ranges"][k]["cidr_ip"]

    new_ip_permissions.append(newipp)

def addSGpermissions(ip_permission, new_ip_permissions, translation):
  for k in range(len(ip_permission["user_id_group_pairs"])):
    # copy all attributes to new ip_permission
    newipp = {}
    newipp["proto"]=ip_permission['proto']
    if "from_port" in ip_permission:
      newipp["from_port"]=ip_permission["from_port"]
    if "to_port" in ip_permission:
      newipp["to_port"]=ip_permission["to_port"]
    newipp["group_id"]=ip_permission["user_id_group_pairs"][k]["group_id"]
    # -> translate sg id's
    newipp["group_id"]=translation[newipp["group_id"]]
    # no sort order has been introduced based on group id...
    # if using names it should be ok, if using group_id it's a problem
    new_ip_permissions.append(newipp)


def addIpv6permissions(ip_permission, new_ip_permissions):
  for k in range(len(ip_permission["ipv6_ranges"])):
    # copy all attributes to new ip_permission
    newipp = {}
    newipp["proto"]=ip_permission['proto']
    if "from_port" in ip_permission:
      newipp["from_port"]=ip_permission["from_port"]
    if "to_port" in ip_permission:
      newipp["to_port"]=ip_permission["to_port"]
    newipp["cidr_ip"]=ip_permission["ipv6_ranges"][k]["cidr_ip"]
    # cidr_ip should be changed? if internal not, if external...don't know

    new_ip_permissions.append(newipp)

def sgRegistrationPlaybook(infrastructure, region, translation):
  """
    register all the sgs first (without dependency management)
    then with all sgs registered and named -> add dependency information
    cost 2n = O(n)
  """

  tasks = []
  i = 0

  for sg in infrastructure['security_groups']:

    # current var
    identifier = u'sg' + u"%d" % i
    # save current subnet id to translation table with register value
    translation[sg['group_id']]='{{ ' + identifier + '.group_id'  + ' }}'

    # translate any needed id to translation table value
    sg["vpc_id"]=translation[sg["vpc_id"]]

    # get tags
    mytags = dict()
    if 'tags' in sg:
      for tagname, tagvalue in sg['tags'].items():
          auxdict = {}
          if not tagname.startswith("aws"):
            auxdict[tagname]=tagvalue
            mytags.update(auxdict)

      mytags['cloned'] = 'yes'

    action = [
      OrderedDict(name='Sg\'s | create sg',
        action=OrderedDict(
          module='ec2_group',
          args=dict(
            vpc_id=sg["vpc_id"],
            name=sg["group_name"],
            region=region,
            state='present',
            description=sg["description"],
            #tags=mytags,
            )
          ),
          register=identifier
        ),
        OrderedDict(name='debug sg',action=OrderedDict(
          module='debug',args=dict(var=identifier)
          )
        )
      ]
    i += 1
    tasks.extend(action)

  print("translation table after sg's")
  pp = pprint.PrettyPrinter(indent=4)
  pp.pprint(translation)
  return tasks

def sgPermissionsPlaybook(infrastructure, region, translation):
  """
    register all the sgs first (without dependency management)
    then with all sgs registered and named -> add dependency information
    cost 2n = O(n)
  """

  tasks = []
  i = 0

  for sg in infrastructure['security_groups']:

    # current var
    identifier = u'sg' + u"%d" % i

    # translate is no longer needed! has been done before

    #ip_permissions
    new_ip_permissions = []
    for j in range(len(sg["ip_permissions"])):

      # change ip_protocol to proto
      sg["ip_permissions"][j]['proto']=sg["ip_permissions"][j]['ip_protocol']

      # change user_id_group_pairs to group_id
      # multiple ip by rule-> turn into a new ip_permissions item
      addSGpermissions(sg["ip_permissions"][j], new_ip_permissions, translation)
      # ipv4  multiple ip_ranges -> turn into a new ip_permissions item
      addIpv4permissions(sg["ip_permissions"][j], new_ip_permissions)
      #ipv6
      addIpv6permissions(sg["ip_permissions"][j], new_ip_permissions)


    #ip_permissions_egress
    new_ip_permissions_egress = []
    for j in range(len(sg["ip_permissions_egress"])):

      # change ip_protocol to proto
      sg["ip_permissions_egress"][j]['proto']=sg["ip_permissions_egress"][j]['ip_protocol']

      # change user_id_group_pairs to group_id
      # multiple -> turn into a new ip_permissions item
      addSGpermissions(sg["ip_permissions_egress"][j], new_ip_permissions_egress, translation)
      # multiple ip_ranges -> turn into a new ip_permissions item
      # use only cidr_ip
      addIpv4permissions(sg["ip_permissions_egress"][j], new_ip_permissions_egress)
      #ipv6
      addIpv6permissions(sg["ip_permissions_egress"][j], new_ip_permissions_egress)

    # get tags
    mytags = dict()
    if 'tags' in sg:
      for tagname, tagvalue in sg['tags'].items():
          auxdict = {}
          if not tagname.startswith("aws"):
            auxdict[tagname]=tagvalue
            mytags.update(auxdict)

      mytags['cloned'] = 'yes'

    action = [
      OrderedDict(name='Sg\'s | create sg',
        action=OrderedDict(
          module='ec2_group',
          args=dict(
            vpc_id=sg["vpc_id"],
            name=sg["group_name"],
            region=region,
            state='present',
            description=sg["description"],
            rules=new_ip_permissions,
            rules_egress=new_ip_permissions_egress,
            #tags=mytags,
            )
          ),
          register=identifier
        ),
      OrderedDict(name='debug sg',action=OrderedDict(
        module='debug',args=dict(var=identifier)
        )
      )
    ]

    i += 1
    tasks.extend(action)

  return tasks

def sgPlaybook(infrastructure, region, translation):
  """
    register all the sgs first (without dependency management)
    then with all sgs registered and named -> add dependency information
    cost 2n = O(n)
  """
  tasks = []
  tasks.extend(sgRegistrationPlaybook(infrastructure, region, translation))
  tasks.extend(sgPermissionsPlaybook(infrastructure, region, translation))
  return tasks


def instanceImagesPlaybook(infrastructure, region, translation):

  tasks = []
  i = 0

  # get first the instasnces that appear in any route

  for instance in infrastructure['instances']:

    # print("processing instance")
    # pp = pprint.PrettyPrinter(indent=2)
    # pp.pprint(instance)

    # create AMI task (put these tasks at the beginning of the playbook!)

    # current var
    identifier = u'AMI' + u"%d" % i
    # save current instance id to translation table with register value
    translation[instance['image_id']]='{{ ' + identifier + '.id'  + ' }}'

    aminame = identifier  + '_' + instance['tags']['Name'] if 'Name' in instance['tags'] else ' '

    action = [
      OrderedDict(name="AMIs | create AMI%d" % i,
        action=OrderedDict(
          module='ec2_ami',
          args=dict(
            delete_snapshot='yes',
            instance_id=instance['id'],
            name=aminame,
            no_reboot='no',
            region=region,
            state='present',
            wait='yes',
            wait_timeout='600'
            )
          ),
          register=identifier,
          #notify=identifier + '_' + instance['id']
        ),
      OrderedDict(name='debug AMI',action=OrderedDict(
          module='debug',args=dict(var=identifier)
          )
        )
      ]

    # attributes to change
    # vpc, (not needed)
    # groups
    # ami-id
    # instance profile (future)
    # subnet_id


    # attributes to use
    # public_ip_address,
    # block_device_mapping?
    # key_name
    # placement (same zone or nevermind?)
    # tags
    # private_ip_address


    # current var
    identifier = u'instance' + u"%d" % i
    # save current instance id to translation table with register value
    translation[instance['id']]="{{" + identifier + "." + "instances[0].id" + "}}"


    print("translation table after insert %s !" % instance['id'])
    pp = pprint.PrettyPrinter(indent=2)
    pp.pprint(translation)

    # get tags
    mytags = dict()
    for tagname, tagvalue in instance['tags'].items():
        auxdict = {}
        if not tagname.startswith("aws"):
          auxdict[tagname]=tagvalue
          mytags.update(auxdict)
    mytags['cloned'] = 'yes'

    mygroups = []
    for group in instance['groups']:
      # get from translation table

      print("group", group['id'],"appending to groups",translation[group['id']])
      mygroups.append(translation[group['id']])

    mysubnet = findSubnetFromPrivateIp(instance['private_ip_address'],instance['vpc_id'], infrastructure, translation)
    mysubnet = translation[mysubnet]

    assignpublicip = instance['assign_public_ip'] if 'assign_public_ip' in instance else 'yes'

    # get the ami_id from the registered var of ec2_ami_find
    ami_identifier = 'ami_find' + u"%d" % i
    translation[instance['image_id']] = '{{ ' + ami_identifier + '.results[0].ami_id }}'

    action2 = [
      OrderedDict(name=" instances | pause 2 min before searching for AMI%d" % i,
         action=OrderedDict(
          module='pause',
          args=dict(
            minutes='2',
            )
          )
        ),
      OrderedDict(name=" instances | find AMI%d" % i,
         action=OrderedDict(
          module='ec2_ami_find',
          args=dict(
            owner='self',
            name=aminame,
            region=region,
            )
          ),
         register=ami_identifier ,
        ),
      OrderedDict(name=" instances | create instance%d" % i,
         action=OrderedDict(
          module='ec2',
          args=dict(
            key_name=instance['key_name'],
            private_ip=instance['private_ip_address'],
            group_id=mygroups,
            image=translation[instance['image_id']],
            region=region,
            wait='yes',
            wait_timeout='600',
            exact_count=1,
            count_tag='id',
            instance_tags=mytags,
            vpc_subnet_id=mysubnet,
            assign_public_ip=assignpublicip,
            instance_type=instance['instance_type'],
            source_dest_check=instance['source_destination_check']
            )
          ),
         register=identifier,
        ),
       OrderedDict(name='debug Instance%d' % i,action=OrderedDict(
          module='debug',args=dict(var=identifier)
          )
        )
    ]


    i += 1
    tasks.extend(action)
    tasks.extend(action2)


  print("translation table after instances!")
  pp = pprint.PrettyPrinter(indent=2)
  pp.pprint(translation)

  # print(" instance actions")
  # pp = pprint.PrettyPrinter(indent=2)
  # pp.pprint(tasks)

  return tasks

def separateRouteableInstances(infrastructure, region, translation):

  # prepare the infrastructure with only instances that appear in routes
  infrastructure_routeable = {}
  infrastructure_routeable['instances'] = []
  infrastructure_routeable['subnets'] = infrastructure['subnets']
  infrastructure_non_routeable = {}
  infrastructure_non_routeable['instances'] = []
  infrastructure_non_routeable['subnets'] = infrastructure['subnets']

  #prepare list
  instances_from_routes = [ instance for route  in infrastructure['routes'] for instance in route['dependencies']['instances'] ]

  for instance in infrastructure['instances']:
    if instance['id'] in instances_from_routes:
        infrastructure_routeable['instances'].append(instance)
    else:
        infrastructure_non_routeable['instances'].append(instance)

  return infrastructure_routeable, infrastructure_non_routeable





def routePlaybook(infrastructure, region, translation):


  tasks = []

  i = 0

  for route in infrastructure['routes']:

    print(" route ",i)
    pp = pprint.PrettyPrinter(indent=4)
    pp.pprint(route)
    print("")

    # current var
    identifier = u'route' + u"%d" % i
    # save current subnet id to translation table with register value
    translation[route['id']]='{{ ' + identifier + '.id'  + ' }}'

    # translate vpcs
    route["vpc_id"]=translation[route["vpc_id"]]

    # translate subnets
    mysubnets = []
    for assoc in route["associations"]:
      print("route",route['id'],"subnet association",assoc["subnet_id"])
      if assoc["subnet_id"]:
        assoc["subnet_id"]=translation[assoc["subnet_id"]]
        mysubnets.append(assoc["subnet_id"])

    # translate routes
    myroutes = []
    for j in range(len(route["routes"])):
      # modify instance_id identifier only
      if route["routes"][j]['instance_id'] and route["routes"][j]['instance_id'] != 'None' :
        route["routes"][j]['instance_id'] = translation[route["routes"][j]['instance_id']]
        myroutes.append({
          'dest': route["routes"][j]["destination_cidr_block"],
          'instance_id': route["routes"][j]["instance_id"]
          })
      else:

        # only transform non local routes
        if route["routes"][j]['gateway_id'] != u'local':
          route["routes"][j]['gateway_id'] = translation[route["routes"][j]['gateway_id']]

        myroutes.append({
          'dest': route["routes"][j]["destination_cidr_block"],
          'gateway_id': route["routes"][j]["gateway_id"]
          })

    print("myroutes", myroutes)

    # get tags
    mytags = dict()
    for tagname, tagvalue in route['tags'].items():
        auxdict = {}
        if not tagname.startswith("aws"):
          auxdict[tagname]=tagvalue
          mytags.update(auxdict)
    mytags['cloned'] = 'yes'

    action = [
      OrderedDict(name="Routes | create route%d" % i,
        action=OrderedDict(
          module='ec2_vpc_route_table',
          args=dict(
            region=region,
            vpc_id=route["vpc_id"],
            lookup='tag',
            state='present',
            subnets=mysubnets,
            routes=myroutes,
            tags=mytags,
            )
          ),
          register=identifier
        ),
      OrderedDict(name='debug Route%d' % i,action=OrderedDict(
          module='debug',args=dict(var=identifier)
          )
        )
    ]

    i += 1

    tasks.extend(action)


  return tasks


def generatePlaybook(infrastructure, region, translation):

  pp = pprint.PrettyPrinter(indent=2)

  tasks = []

  tasks.extend(vpcPlaybook(infrastructure, region, translation))
  tasks.extend(igwPlaybook(infrastructure, region, translation))
  tasks.extend(subnetPlaybook(infrastructure, region, translation))
  tasks.extend(naclPlaybook(infrastructure, region, translation))
  tasks.extend(sgPlaybook(infrastructure, region, translation))

  routeable_instances, non_routeable_instances = separateRouteableInstances(infrastructure, region, translation)

  print("")
  print("routeable instances")
  pp.pprint(routeable_instances)
  print("")
  print("non routeable instances")
  pp.pprint(non_routeable_instances)

  tasks.extend(instanceImagesPlaybook(routeable_instances, region, translation))
  tasks.extend(routePlaybook(infrastructure, region, translation))
  tasks.extend(instanceImagesPlaybook(non_routeable_instances, region, translation))

  print(" all actions")
  pp.pprint(tasks)

  return tasks





# Playbook generation as a folder structure -----------------------------------

