# -*- coding: utf-8 -*-


from __future__ import (absolute_import, division, print_function)

import json
from collections import namedtuple
import subprocess
import boto3
import yaml
import pyaml
import pprint
import os
import sys


import ansible
from ansible.parsing.dataloader import DataLoader
from ansible.vars import VariableManager
from ansible.inventory import Inventory
from ansible.inventory import group
from ansible.playbook.play import Play
from ansible.executor.task_queue_manager import TaskQueueManager
from ansible.plugins.callback.json import CallbackModule
from ansible.plugins.callback import CallbackBase



# Facts gathering actions ---------------------------------------------------

def vpcFactsAction(region):

  action = [
     dict(
      name="vpc_facts",
      action=dict(
        module='ec2_vpc_net_facts',
        args=dict(region=region,
        # filters=dict(
        #   {
        #     #"tag:Name": "test_vpc",
        #     "cidr": "10.0.0./16"
        #   })

        # add subnet, routetable, route, igw, sg, nacl
        # add instance,
        )
      ),
      register='vpc_gather'),
    #dict(action=dict(module='debug', args=dict(var='vpc_gather')))
    ]

  return action

def igwFactsActions(region):

  # no plugin implemented!

  action = [
    dict(name="igw_facts",
      action=dict(
        module='ec2_vpc_igw_facts',
        args=dict(
          region=region)
        ),
      register='igw_gather')

  ]

  return action


def subnetsFactsActions(region):

  action = [
    dict(name="subnet_facts",
      action=dict(
        module='ec2_vpc_subnet_facts',
        args=dict(
          region=region)
        ),
      register='subnets_gather'),
    #dict(action=dict(module='debug', args=dict(var='subnets_gather')))
  ]

  return action

def naclsFactsActions(region):

  action = [

    dict(name="nacls_facts",
      action=dict(
        module='ec2_vpc_nacl_facts',
        args=dict(
          region=region)
        ),
      register='nacls_gather'),
    #dict(action=dict(module='debug', args=dict(var='nacls_gather')))

  ]

  return action

def securityGroupsFactsActions(region):

  action = [

    dict(name="sg_facts",
      action=dict(
        module='ec2_group_facts',
        args=dict(region=region)
        ),
        register='sgs_gather'
      ),
    dict(action=dict(module='debug', args=dict(var='sgs_gather'))),
  ]

  return action

def instancesFactsActions(region):

  action = [
    dict(name="ec2_facts",
      action=dict(
        module='ec2_remote_facts',
        args=dict(
          region=region,
          #filters=['instance-state-name: running']
          )
        ),
        register='instances_gather'
      ),
    #dict(action=dict(module='debug', args=dict(var='instances_gather'))),
  ]

  return action

def routesFactsActions(region):

  action = [
    dict(name="routes_facts",
      action=dict(
        module='ec2_vpc_route_table_facts',
        args=dict(region=region)
        ),
        register='routes_gather'
      ),
    #dict(action=dict(module='debug', args=dict(var='routes_gather'))),

  ]

  return action


def factsActions(region):

  tasks = []

  tasks.extend(vpcFactsAction(region))
  tasks.extend(igwFactsActions(region))
  tasks.extend(subnetsFactsActions(region))
  tasks.extend(naclsFactsActions(region))
  tasks.extend(securityGroupsFactsActions(region))
  tasks.extend(instancesFactsActions(region))
  tasks.extend(routesFactsActions(region))

  #print(tasks)

  return tasks




