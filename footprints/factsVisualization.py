# -*- coding: utf-8 -*-


from __future__ import (absolute_import, division, print_function)

import json
from collections import namedtuple
import subprocess
import boto3
import yaml
import pyaml
import pprint
import os
import sys


import ansible
from ansible.parsing.dataloader import DataLoader
from ansible.vars import VariableManager
from ansible.inventory import Inventory
from ansible.inventory import group
from ansible.playbook.play import Play
from ansible.executor.task_queue_manager import TaskQueueManager
from ansible.plugins.callback.json import CallbackModule
from ansible.plugins.callback import CallbackBase



# Visualization -----------------------------------------------------------


def printPlaybook(playbook):
    pp = pprint.PrettyPrinter(indent=4)
    pp.pprint(playbook)

def printPickedFacts(json_callback):
    #print(json_callback.results[0]['tasks'])
    #print("")

    for task in json_callback.results[0]['tasks']:

      print(task['hosts']['localhost'].keys()[:2])

      if task['task']['name'] == "vpc_facts":
        elems = "vpcs"
        theid = "Name"
      elif task['task']['name'] == "igw_facts":
        elems = "internet_gateways"
        theid = "internet_gateway_id"
      elif task['task']['name'] == "subnet_facts":
        elems = "subnets"
        theid = "Name"
      elif task['task']['name'] == "nacls_facts":
        elems = "nacls"
        theid = "nacl_id"
      elif task['task']['name'] == "ec2_facts":
        elems = "instances"
        theid = "Name"
      elif task['task']['name'] == "routes_facts":
        elems = "route_tables"
        theid = "id"
      elif task['task']['name'] == "sg_facts":
        elems = "security_groups"
        theid = "group_id"
      else:
        print(json_callback.results[0]['tasks'])
        print("")


      print(task['task']['name'],task['hosts']['localhost']['invocation']['module_name'])
      #print(task['hosts']['localhost'])
      for elem in task['hosts']['localhost'][elems]:
        try:
          print("        "+elem[theid])
        except:
          print("        "+elem['tags'][theid])

def printInfrastructure(infrastructure):

  # easy way
  pp = pprint.PrettyPrinter(indent=4)
  pp.pprint(infrastructure)

def printDependenciesSubTree(dependencies):
  for k,v in dependencies.items():
    if len(v) > 0:
      print("    ","    ",k,v)

def printDependencies(infrastructure):

  # parse for each elem the dependencies and the identifier
  print("vpcs:")
  for elem in infrastructure['vpcs']:
    print("    ",elem['id'],elem['cidr_block'],elem['tags']['Name'])

  print("igws:")
  for elem in infrastructure['igws']:
    print("    ",elem['internet_gateway_id'])
    print("    ","  ","depends on:")
    printDependenciesSubTree(elem['dependencies'])

  print("subnets:")
  for elem in infrastructure['subnets']:
    print("    ",elem['id'])
    print("    ","  ","depends on:")
    printDependenciesSubTree(elem['dependencies'])

  print("nacls:")
  for elem in infrastructure['nacls']:
    print("    ",elem['nacl_id'])
    print("    ","  ","depends on:")
    printDependenciesSubTree(elem['dependencies'])

  print("routes:")
  for elem in infrastructure['routes']:
    print("    ",elem['id'])
    print("    ","  ","depends on:")
    printDependenciesSubTree(elem['dependencies'])


  print("security_groups:")
  for elem in infrastructure['security_groups']:
    print("    ",elem['group_id'])
    print("    ","  ","depends on:")
    printDependenciesSubTree(elem['dependencies'])

  print("instances:")
  for elem in infrastructure['instances']:
    print("    ",elem['id'])
    print("    ","  ","depends on:")
    printDependenciesSubTree(elem['dependencies'])
