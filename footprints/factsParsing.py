# -*- coding: utf-8 -*-
from __future__ import (absolute_import, division, print_function)

import json
from collections import namedtuple
import subprocess
import boto3
import yaml
import pyaml
import pprint
import os
import sys


import ansible
from ansible.parsing.dataloader import DataLoader
from ansible.vars import VariableManager
from ansible.inventory import Inventory
from ansible.inventory import group
from ansible.playbook.play import Play
from ansible.executor.task_queue_manager import TaskQueueManager
from ansible.plugins.callback.json import CallbackModule
from ansible.plugins.callback import CallbackBase
import boto3

# Facts parsing ---------------------------------------------------

def loadConfigFile(filepath):

  confFile = os.path.join('./',filepath)
  conf = yaml.load(file(confFile,'r'))
  return conf



def recursiveFindParentNodeForValue(value, mydict):

  """
      return the most high level element that contains it
      this is return mydict or list elem or return None
  """

  if isinstance(mydict, list):
    for elem in mydict:
      #print("    "," list looking in :",elem)
      subkey = recursiveFindParentNodeForValue(value, elem)
      if subkey:

        return elem

  elif isinstance(mydict, dict):
    for k,v in mydict.items():
      #print("    "," dict looking in :",k," comparison with",v)
      subkey = recursiveFindParentNodeForValue(value, v)
      if subkey:
        return mydict

  elif isinstance(mydict, (int, long, float, str)):
    #print("    ",value," element comparing with numeric :",mydict)
    if value == mydict:
      return mydict

  elif isinstance(mydict, unicode):
    #print("    ",value," element comparing with str :",mydict)
    if unicode(value) == mydict:
      return mydict

  #print("    "," -> not found")
  return None


def findInstanceIdFromEnidId(eniid, infrastructure):

  # look into instances for the eni-id identifier
  # so look for the TOP parent node that contains this value in one of its childs
  node = recursiveFindParentNodeForValue(eniid, infrastructure['instances'])

  # and retrieve the instance id
  if node:
    return node["id"]
  else:
    return None

def ipNumFromString(ip):

    if isinstance(ip, basestring):
      #  private ip to bytes
      octet1 = ip[:ip.find(".")]
      ip = ip[ip.find(".")+1:]
      octet2 = ip[:ip.find(".")]
      ip = ip[ip.find(".")+1:]
      octet3 = ip[:ip.find(".")]
      ip = ip[ip.find(".")+1:]
      octet4 = ip

      #print("octet1",octet1,"octet2",octet2,"octet3",octet3,"octet4",octet4)
      #print("octet1",(int(octet1)*pow(2,24)),"octet2",(int(octet2)*pow(2,16)),"octet3",(int(octet3)*pow(2,8)),"octet4",int(octet4))

      return int(octet4) + int(octet3)*pow(2,8) + int(octet2)*pow(2,16) + int(octet1)*pow(2,24)

    return None

def ipStringFromNum(ip):

    if isinstance(ip, (int, long, float)):
      #  private ip to bytes
      octet1 = int(ip / pow(2,24))
      #print("octet1",octet1)

      octet2 = int(ip / pow(2,16))  - octet1*pow(2,8)
      #print("octet2",octet2)

      octet3 =  int(ip / pow(2,8) ) - octet1*pow(2,16) - octet2*pow(2,8)
      #print("octet3",octet3)

      octet4 = ip - octet1*pow(2,24) - octet2*pow(2,16) - octet3*pow(2,8)
      #print("octet1*pow(2,24) + octet2*pow(2,16) + octet3*pow(2,8)",(octet1*pow(2,24) + octet2*pow(2,16) + octet3*pow(2,8)))
      #print("octet4",octet4)

      return "%d.%d.%d.%d" % (int(octet1), int(octet2), int(octet3), int(octet4))

    return None



def findSubnetFromPrivateIp(instance_privateip, instance_vpc_id, infrastructure, translation={}):

  privateip = instance_privateip
  # look into subnets for the cidr_block
  # see what cidr_block encloses the current private ip
  if not privateip:
    return None

  for subnet in infrastructure["subnets"]:
    # get cidr and mask
    cidr = subnet['cidr_block']
    masks = cidr[cidr.find("/")+1:]
    mask = int(masks)
    network = cidr[:cidr.find("/")]

    # apply mask to privateip
    privateipnum = ipNumFromString(privateip)
    #print("findSubnetFromPrivateIp",privateip, privateipnum)
    #print("private ip num", privateipnum)
    mask2 = 32 - mask
    privateipmasked = ( int( privateipnum / pow(2,mask2) ) * pow(2,mask2) )
    #print("private ip masked" , privateipmasked)
    privateipmasked_string = ipStringFromNum(privateipmasked)
    #print("private ip masked to string" , privateipmasked_string)

    #print("cidr",cidr,"network", network,"mask",masks,"ip",privateip, "ip masked",privateipmasked_string)

    # if masked priavte ip = network => MATCH! subnet found
    if privateipmasked_string == network:
      print("cidr",cidr,"network", network,"mask",masks,"ip",privateip, "ip masked",privateipmasked_string, "instance_vpc_id", instance_vpc_id, "subnet_vpc",subnet['vpc_id'])

      # must also match that the vpc of the subnet and the instance is the same!
      # get vpc of the instances -> instance_vpc_id

      # get vpc of the subnet -> subnet['vpc_id']

      # if both vpc's are the same -> return the id
      if subnet['vpc_id'] == instance_vpc_id or len(translation.items()) > 0 and translation[instance_vpc_id] == subnet['vpc_id']:
        return subnet['id']


  return None


def newDependencies():
  dependencies = {
    "vpcs": [],
    "igws": [],
    "subnets": [],
    "routes": [],
    "nacls": [],
    "security_groups": [],
    "instances": [],
    "enis": []
    }

  return dependencies


def parseVPCFacts(vpcs, infrastructure):

  for elem in vpcs:

    dependencies = newDependencies()

    # add dependencies struct
    elem['dependencies'] = dependencies

    # add elem to infrasctructure
    infrastructure['vpcs'].append(elem)


def parseIGWFacts(igws, infrastructure):

  for elem in igws:

    dependencies = newDependencies()

    # parse vpc dependencies
    for attach in elem['attachments']:
      dependencies['vpcs'].append(attach['vpc_id'])

    # add dependencies struct
    elem['dependencies'] = dependencies

    # add elem to infrasctructure
    infrastructure['igws'].append(elem)


def parseSubnetFacts(subnets, infrastructure):
  for elem in subnets:

    dependencies = newDependencies()

    # parse vpc dependencies
    dependencies['vpcs'].append(elem['vpc_id'])

    # parse igw dependencies

    # parse routes dependencies

    # parse nacl dependencies

    # add dependencies struct
    elem['dependencies'] = dependencies

    # add elem to infrasctructure
    infrastructure['subnets'].append(elem)


def parseNACLFacts(nacls, infrastructure):
  for elem in nacls:

    dependencies = newDependencies()

    # parse vpc dependencies
    dependencies['vpcs'].append(elem['vpc_id'])

    # parse igw dependencies

    # parse routes dependencies

    # parse subnets dependencies
    for subnet in elem['subnets']:
      dependencies['subnets'].append(subnet)

    # add dependencies struct
    elem['dependencies'] = dependencies

    # add elem to infrasctructure
    infrastructure['nacls'].append(elem)

def addInstanceType(instanceDict):

  ec2 = boto3.resource('ec2')
  instance = ec2.Instance(instanceDict['id'])
  instanceDict['instance_type'] = instance.instance_type
  return instanceDict



def parseInstanceFacts(instances, infrastructure):
  for elem in instances:

    dependencies = newDependencies()

    # parse vpc dependencies
    dependencies['vpcs'].append(elem['vpc_id'])

    # parse sg dependencies
    for sgs in elem['groups']:
      dependencies['security_groups'].append(sgs['id'])


    # parse subnets dependencies
    # from private ip address found the subnet
    # what to do when multiple eni's¿ test and see what is the datastruct
    privip = elem['private_ip_address']
    subnetid = findSubnetFromPrivateIp(privip, elem['vpc_id'], infrastructure)
    if subnetid:
      dependencies['subnets'].append(subnetid)
      dependencies['subnets'].append(privip)
    else:
      dependencies['subnets'].append(privip)

    # add dependencies struct
    elem['dependencies'] = dependencies


    #get instance type before adding to infrastructure
    elem = addInstanceType(elem)

    # add elem to infrasctructure
    infrastructure['instances'].append(elem)



def parseRouteFacts(routes, infrastructure):
  for elem in routes:

    dependencies = newDependencies()

    # parse vpc dependencies
    dependencies['vpcs'].append(elem['vpc_id'])


    for route in elem['routes']:

      # parse igw dependencies
      if route['gateway_id'] != "local" and route['gateway_id']:
        dependencies['igws'].append(route['gateway_id'])

      # parse instance dependencies
      if route['instance_id'] != "None" and route['instance_id']:
        dependencies['instances'].append(route['instance_id'])

      # parse instance dependencies
      if route['interface_id'] != "None" and route['interface_id']:
        # at that point one should found the instance associated with the eni-id
        #print("looking for ",route['interface_id'])
        instance_id = findInstanceIdFromEnidId(route['interface_id'], infrastructure)
        if instance_id:

          # should the eni-id also be saved?,
          # (it would change when new vpc created, so no for the moment)

          if instance_id not in dependencies['instances']:
            dependencies['instances'].append(instance_id)
        else:
          dependencies['enis'].append(route['interface_id'])


    # add dependencies struct
    elem['dependencies'] = dependencies

    # add elem to infrasctructure
    infrastructure['routes'].append(elem)


def parseSgDependencies(sg):
  #dependencies list
  dependencies=[]

  # get all its sg dependencies from ip_permissions
  # filter out dependencies that have already appeared
  for entry in sg['ip_permissions']:
    if len(entry['user_id_group_pairs']) > 0:
      extralist = [ x['group_id'] for x in entry['user_id_group_pairs'] if x['group_id'] != sg['group_id'] and x['group_id'] not in dependencies ]
      dependencies.extend(extralist)

  # get all its sg dependencies from ip_permissions_egress
  # filter out dependencies that have already appeared
  for entry in sg['ip_permissions_egress']:
    if len(entry['user_id_group_pairs']) > 0:
      extralist = [ x['group_id'] for x in entry['user_id_group_pairs'] if x['group_id'] != sg['group_id'] and x['group_id'] not in dependencies ]
      dependencies.extend(extralist)

  return dependencies



def parseSGFacts(security_groups, infrastructure):
  for elem in security_groups:

    dependencies = newDependencies()

    # parse vpc dependencies
    dependencies['vpcs'].append(elem['vpc_id'])

    # parse igw dependencies

    # parse routes dependencies

    # parse subnets dependencies

    # parse sg dependencies
    dependencies['security_groups'] = parseSgDependencies(elem)

    # add dependencies struct
    elem['dependencies'] = dependencies

    # add elem to infrasctructure
    infrastructure['security_groups'].append(elem)



def parseFacts(json_callback):

  """
      1. create empty datastruct
      2. load identifiers and references from disk
      3. parse results for each type (vpcs, subnets, instances)
      4. get for each element all the attributes dict
      5. parse attributes dict to find cross references to other elems
      6. fill the "dependencies" lists with every elem identifier in its own type list
  """

  # 1. create empty datastruct
  infrastructure = {
    "vpcs": [],
    "igws": [],
    "subnets": [],
    "routes": [],
    "nacls": [],
    "security_groups": [],
    "instances": []
  }

  # 2. load identifiers and references from config file
  identifiers = loadConfigFile('identifiers.yml')
  references = loadConfigFile('references.yaml')



  # 3. parse results for each type
  for elem_group in json_callback.results[0]['tasks']:

    # get the type and prepare the parsing
    group_name = elem_group['task']['name']

    if group_name == "vpc_facts":
      elems = "vpcs"
      parseVPCFacts(elem_group['hosts']['localhost'][elems], infrastructure)
    elif group_name == "igw_facts":
      elems = "internet_gateways"
      parseIGWFacts(elem_group['hosts']['localhost'][elems], infrastructure)
    elif group_name == "subnet_facts":
      elems = "subnets"
      parseSubnetFacts(elem_group['hosts']['localhost'][elems], infrastructure)
    elif group_name == "nacls_facts":
      elems = "nacls"
      parseNACLFacts(elem_group['hosts']['localhost'][elems], infrastructure)
    elif group_name == "ec2_facts":
      elems = "instances"
      parseInstanceFacts(elem_group['hosts']['localhost'][elems], infrastructure)
    elif group_name == "routes_facts":
      elems = "route_tables"
      parseRouteFacts(elem_group['hosts']['localhost'][elems], infrastructure)
    elif group_name == "sg_facts":
      elems = "security_groups"
      parseSGFacts(elem_group['hosts']['localhost'][elems], infrastructure)
    else:
      pass

  return infrastructure


def writeFactsToYaml(infrastructure):
  """
    save to a yaml file all infrastructure elements
  """

  selectionfile = open('selection.yml','w')
  selectionfile.write("---" + os.linesep)

  for category, elements in infrastructure.items():

      selectionfile.write(category+":"+ os.linesep)
      for element in elements:
          if 'tags' in element \
              and len(element["tags"]) > 0 \
              and 'Name' in element['tags']:
            selectionfile.write("  #"+element['tags']['Name']+ os.linesep)

          if category == "igws":
            selectionfile.write("  - "+element['internet_gateway_id']+ os.linesep)
          elif category == "instances":
            selectionfile.write("  - "+element['id']+ os.linesep)
          elif category == "nacls":
            selectionfile.write("  - "+element['nacl_id']+ os.linesep)
          elif category == "routes":
            selectionfile.write("  - "+element['id']+ os.linesep)
          elif category == "security_groups":
            if 'group_name' in element :
              selectionfile.write("  #"+element['group_name']+ os.linesep)
            selectionfile.write("  - "+element['group_id']+ os.linesep)
          elif category == "subnets":
            selectionfile.write("  - "+element['id']+ os.linesep)
          elif category == "vpcs":
            selectionfile.write("  - "+element['id']+ os.linesep)

      selectionfile.write(""+ os.linesep )

  selectionfile.close()


def filterInfrastructure(infrastructure, selected_items_list):
  """
    Input: a list with dicts like {"type":"vpc", "id":"vpc-231231"}
    Output: Infrastructure data struct with only selecte items and it's relatives/dependencies

    allows to choose one or more elements, and only those elements and their dependencies and their contained elements will remain in the infrastructure.

    ex:
       when choosing a vpc, all subnets, igw, nacls, routes, sg and instances contained in it will remain in the infrastructure datastruct

       when choosing an instance, its subnet, nacls, sg, routetable, igw and vpc and the instance itself will remain in the infrastructure datastruct

       when choosing a subnet, all the instances contained in it , and also all dependencies of the subnet and instances (nacl, sg, route, igw, vpc) will remain in the infrastructure datastruct

  """




