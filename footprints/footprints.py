# -*- coding: utf-8 -*-
from __future__ import (absolute_import, division, print_function)

import json
from collections import namedtuple
import subprocess
import boto3
import yaml
import pyaml
import pprint
import os
import sys
import argparse

from factsGathering import *
from factsVisualization import *
from factsParsing import *
from factsFiltering import *
from playbookRunner import *
from playbookGenerator import *
from playbookWriter import *


# functionalities ----------------------------------------------------------



# Main functions -------------------------------------------------------------

def clone(region):

  # get facts of everything in an AWS region
  facts = provisionPlaybook(factsActions(region))


  infrastructure = None
  if facts.results and \
    len(facts.results) > 0 and \
    facts.results[0]['tasks']:

    printPlaybook(facts.results[0]['tasks'])

    # transform ansible facts to infrastructure datastruct
    infrastructure = parseFacts(facts)

    #apply a selection to discard unselected items and their dependedants
    selection = getFactsSelection()
    infrastructure = filterFacts(infrastructure, selection)

    # Text visualization of infrastructure and dependencies
    print("")
    print("infrastructure (gathered facts)")
    printInfrastructure(infrastructure)
    print("")
    print("dependencies")
    printDependencies(infrastructure)

  # after gathering facts saved into infrastructure
  # -> gen and run playbook
  if infrastructure:

    # prepare empty translation dict
    translation = {}

    # write playbook dict()
    myplaybook = generatePlaybook(infrastructure, region, translation)
    #printPlaybook(myplaybook)

    writePlaybookFile(myplaybook)

    # provision playbook (best practice)
    provision = provisionPlaybook(myplaybook)
    print("")
    print("cloning playbook result")
    printPlaybook(provision.results)


def generateCloningPlaybook(region):

  # get facts of everything in an AWS region
  facts = provisionPlaybook(factsActions(region))


  infrastructure = None
  if facts.results and \
    len(facts.results) > 0 and \
    facts.results[0]['tasks']:

    printPlaybook(facts.results[0]['tasks'])

    # transform ansible facts to infrastructure datastruct
    infrastructure = parseFacts(facts)

    #apply a selection to discard unselected items and their dependedants
    selection = getFactsSelection()
    infrastructure = filterFacts(infrastructure, selection)

    # Text visualization of infrastructure and dependencies
    print("")
    print("infrastructure (gathered facts)")
    printInfrastructure(infrastructure)
    print("")
    print("dependencies")
    printDependencies(infrastructure)

  # after gathering facts saved into infrastructure
  # -> gen and run playbook
  if infrastructure:

    # prepare empty translation dict
    translation = {}

    # write playbook dict()
    myplaybook = generatePlaybook(infrastructure, region, translation)
    #printPlaybook(myplaybook)

    writePlaybookFile(myplaybook)



def factsToYaml(region):

  # get facts of everything in an AWS region
  facts = provisionPlaybook(factsActions(region))

  infrastructure = None
  if facts.results and \
    len(facts.results) > 0 and \
    facts.results[0]['tasks']:

    # transform ansible facts to infrastructure datastruct
    infrastructure = parseFacts(facts)
    writeFactsToYaml(infrastructure)




if __name__ == '__main__':

  parser = argparse.ArgumentParser(description='Get all the events matching a value.')
  parser.add_argument("-s", "--selection", action='store_true', required=False, help="to only get facts to a selection.yml file.")
  parser.add_argument("-p", "--playbook", action='store_true', required=False, help="to only get facts to a selection.yml file.")
  parser.add_argument("-c", "--clone", action='store_true', required=False, help="to only get facts to a selection.yml file.")
  # parser.add_argument("-q", "--quiet", action='store_true', help="Only display URLs to MISP")
  # parser.add_argument("-o", "--output", help="Output file")
  # parser.add_argument("-f", "--fast", action='store_true', help="fast search")
  args = parser.parse_args()

  region = 'eu-west-1'

  if args.playbook:
    generateCloningPlaybook(region)
  elif args.clone:
    clone(region)
  elif args.selection:
    factsToYaml(region)
  else:
    # for the moment this will be the default behaviour (safer)
    factsToYaml(region)
