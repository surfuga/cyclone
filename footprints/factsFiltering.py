# -*- coding: utf-8 -*-
from __future__ import (absolute_import, division, print_function)

import json
from collections import namedtuple
import subprocess
import boto3
import yaml
import pyaml
import pprint
import os
import sys


import ansible
from ansible.parsing.dataloader import DataLoader
from ansible.vars import VariableManager
from ansible.inventory import Inventory
from ansible.inventory import group
from ansible.playbook.play import Play
from ansible.executor.task_queue_manager import TaskQueueManager
from ansible.plugins.callback.json import CallbackModule
from ansible.plugins.callback import CallbackBase
from factsParsing import *

# Facts filtering ---------------------------------------------------


def igwVPCsInSelection(igw, selection):
    if len(igw['attachments']) == 0:
        return False

    result = True
    for att in igw['attachments']:
        result = result and att['vpc_id'] in selection['vpcs']
    return result

def naclSubnetsInSelection(nacl, selection):
    if len(nacl['subnets']) == 0:
        return False

    result = True
    for subnet in nacl['subnets']:
        result = result and subnet in selection['subnets']
    return result

def sgSGsInSelectionInternal(rule, selection):
    result = True
    for groupid in rule:
        result = result and groupid['group_id'] in selection['security_groups']
    return result

def sgSGsInSelection(sg, selection):
    result = True
    # ip_permissions
    for rule in sg['ip_permissions']:
        result = result and \
            (len(rule['user_id_group_pairs']) == 0 or \
             sgSGsInSelectionInternal(rule['user_id_group_pairs'],selection))
    # ip_permissions_egress
    for rule in sg['ip_permissions_egress']:
        result = result and \
            (len(rule['user_id_group_pairs']) == 0 or \
             sgSGsInSelectionInternal(rule['user_id_group_pairs'],selection))
    return result

def routeIGWInSelection(route, selection):
    result = True
    found = False
    for r in route['routes']:
        print("routetable comparison",route["id"]," routes: ",r["gateway_id"]," not in ",selection["igws"],(r["gateway_id"] not in selection['igws']))

        if r["gateway_id"] != u'local' and \
            r["gateway_id"] and \
            r["gateway_id"] not in selection['igws']:
            found = True
            break
    return result and not found

def instanceIpInSubnetSelection(instance_private_ip, instance_vpc_id,  infrastructure, selection):
    mysubnet = findSubnetFromPrivateIp(instance_private_ip, instance_vpc_id,infrastructure)
    return mysubnet in selection["subnets"]


def instanceSGsInSelection(groups, selection):
    result = True
    for group in groups:
        result = result and group["id"] in selection['security_groups']
    return result



def filterFacts(infrastructure, selection):

  output = {
    "vpcs": [],
    "igws": [],
    "subnets": [],
    "routes": [],
    "nacls": [],
    "security_groups": [],
    "instances": [],
    "enis": []
  }

  # vpc fase --------------------------
  """
      1. given the final vpc list,
      pick elements in each list (vpcs, igws, subnets, ...) that depend on it
        1.1 for each element pick vpc_id
        1.2 if this vpc_id in selection["vps"] -> add element to infrastructure output
  """
  output["vpcs"] = [ vpc for vpc in infrastructure['vpcs'] if vpc['id'] in selection['vpcs']]

  # igws fase -------------------------
  # 1 filter igw id
  # 2 filter by vpc id
  output["igws"] = [ igw for igw in infrastructure['igws']
                        if igwVPCsInSelection(igw, selection) and \
                           igw['internet_gateway_id'] in selection['igws']]

  # subnet fase -----------------------
  # 1 filter by subnet id
  # 2 filter by vpc id
  output["subnets"] = [ subnet for subnet in infrastructure['subnets']
                        if subnet['vpc_id'] in selection['vpcs'] and
                           subnet['id'] in selection['subnets']]

  # nacl fase -----------------------
  # 1 filter by nacl id
  # 1 filter by subnet id
  # 2 filter by vpc id
  output["nacls"] = [ nacl for nacl in infrastructure['nacls']
                        if nacl['vpc_id'] in selection['vpcs'] and
                           nacl['nacl_id'] in selection['nacls'] and
                           naclSubnetsInSelection(nacl, selection)]

  # sg fase -----------------------
  # 1 filter by vpc_id
  # 1 filter by group_id
  # 2 filter by group_id in ip_permissions and ip_permissions_egress
  output["security_groups"] = [ sg for sg in infrastructure['security_groups']
                        if sg['vpc_id'] in selection['vpcs'] and
                           sg['group_id'] in selection['security_groups'] and
                           sgSGsInSelection(sg, selection)]

  # instance fase -----------------------
  # 1 filter by id (instance)
  # 1 filter by groups/id
  # 1 filter by private_ip_address (subnet)
  # 2 filter by vpc_id

  print("before filtering instances !" )
  pp = pprint.PrettyPrinter(indent=2)
  pp.pprint(infrastructure['instances'])

  output["instances"] = [ instance for instance in infrastructure['instances']
                        if instance['id'] in selection['instances'] and
                           instance['vpc_id'] in selection['vpcs'] and
                           instanceIpInSubnetSelection(instance['private_ip_address'],instance['vpc_id'], infrastructure, selection) and
                           instanceSGsInSelection(instance["groups"], selection)]

  print("after filtering instances !" )
  pp = pprint.PrettyPrinter(indent=2)
  pp.pprint(output['instances'])

  # route fase -----------------------
  # 1 filter by id
  # 1 filter by vpc_id
  # 3 filter by routes[ {gateway_id} ] / local always accepted
  output["routes"] = [ route for route in infrastructure['routes']
                        if route['vpc_id'] in selection['vpcs'] and
                           route['id'] in selection['routes'] and
                           routeIGWInSelection(route, selection)]
  for route in infrastructure['routes']:
    print("route",
          route["id"],
          " route vpc",
          route['vpc_id'],
          " in vpcs?",
          route['vpc_id'] in selection['vpcs'],
          " in routes?",
          route['id'] in selection['routes'])


  return output


def getFactsSelection():

  selection_file = open("selection.yml",'r')
  selection = yaml.load(selection_file)
  # selection = {
  #   "vpcs": ['vpc-989da8fc'],
  #   "igws": ['igw-76fc5112'],
  #   "subnets": ['subnet-be764cda','subnet-b8764cdc'],
  #   "routes": ['rtb-62458805','rtb-8e4a87e9'],
  #   #"routes": ['rtb-8e4a87e9'],
  #   "nacls": ['acl-3f778258'],
  #   "security_groups": ['sg-2c052f4a','sg-3c052f5a','sg-3f052f59'],
  #   "instances": ['i-0cc57389c18bb5036','i-08d939a157546ac56'],
  #   "enis": []
  # }

  # selections must go on cascade
  # to select an igw, you must select its vpc



  return selection
