# Cyclone

The goal of this project is to build a PoC of an automated pentesting system that clones infrastructure in the AWS cloud and then scans for vulnerabilities using automated powershell scripts.1

## Cloning the infrastructure

The first part of the project, called footprints, clones infrastructure in AWS (VPC, subnets, and instances) generating an ansible playbook to replicate it and then replicating it in the same account but a new VPC.
All CIDRs, security groups, subnet names are preserved in the cloned infrastructure. For the original instances, AMI's are created automatically and then the instances in the cloned infrastrucure are build from those new AMIs.

## Security assessment

The second part of the project, called theredone, launches an automated security assessment tool build on powershell (the famous powersploit) to scas n for vulnerability from inside Windows Servers machines.
The scripts are deployed and run by Ansible inside the server instance.