# -*- coding: utf-8 -*-
from __future__ import (absolute_import, division, print_function)

import json
from collections import namedtuple
import subprocess
import boto3
import yaml
import pyaml
import pprint

import os
import sys
from collections import OrderedDict


import ansible
from ansible.parsing.dataloader import DataLoader
from ansible.vars import VariableManager
from ansible.inventory import Inventory
from ansible.inventory import group
from ansible.playbook.play import Play
from ansible.executor.task_queue_manager import TaskQueueManager
from ansible.plugins.callback.json import CallbackModule
from ansible.plugins.callback import CallbackBase






# Playbook generation as a dict -------------------------------------------

def win_ping():
  action = [
     dict(
      name="win_ping",
      action=dict(
        module='win_ping',
      ),
      register='win_ping'),
     #dict(name="win_ping debug",action=dict(module='debug', args=dict(var='win_ping')))
    ]

  return action


def win_folder():
  action = [
     dict(
      name="win_file",
      action=dict(
        module='win_file',
        state='directory',
        path='c:\Program Files\service 01'
      ),
      register='win_folder'),
     #dict(name="win_ping debug",action=dict(module='debug', args=dict(var='win_ping')))
    ]

  return action


def win_copy():
  action = [
     dict(
      name="win_copy",
      action=dict(
        module='win_copy',
        src='./scripts/dummyService/FolderWatcher.exe',
        dest='c:\Program Files\service 01'
      ),
      register='win_copy'),
     #dict(name="win_ping debug",action=dict(module='debug', args=dict(var='win_ping')))
    ]

  return action

def win_service():
  action = [
     dict(
      name="win_service",
      action=dict(
        module='win_service',
        name='FolderWatcher',
        path='c:\Program Files\service 01\FolderWatcher.exe'
      ),
      register='win_service'),
     #dict(name="win_ping debug",action=dict(module='debug', args=dict(var='win_ping')))
    ]

  return action

def hello_ps():
  action = [
     dict(
      name="hello_ps",
      action=dict(
        module='script',
        args='./scripts/test/Hello.ps1'
      ),
      register='hello_ps',
      failed_when=u"'FAILED' in hello_ps.stdout",
      ignore_errors='true'),
    ]

  return action

def error_ps():
  action = [
     dict(
      name="error_ps",
      action=dict(
        module='script',
        args='./scripts/test/Error.ps1'
      ),
      register='error_ps',
      failed_when=u"'FAILED' in error_ps.stdout",
      #changed_when=u"'FAILED' not in error_ps.stdout",
      ignore_errors='true'),
    ]

  return action

def error2_ps():
  action = [
     dict(
      name="error2_ps",
      action=dict(
        module='script',
        args='./scripts/test/Error2.ps1'
      ),
      register='error2_ps',
      #changed_when=u"error2_ps.rc == 0",
      #changed_when="False",
      #changed_when=False,
      ignore_errors='true'),
     dict(name="error2_ps debug",action=dict(module='debug', args=dict(var='error2_ps')))
    ]

  return action



def extra_ps():
  action = [
     dict(
      name="extra_ps",
      action=dict(
        module='script',
        args='./scripts/test/extracted_powersploit_script_01.ps1'
      ),
      register='extra_ps',
      ignore_errors='true'),
     dict(name="extra_ps debug",action=dict(module='debug', args=dict(var='extra_ps')))
    ]

  return action


def upload_scripts():
  action = [
     dict(
      name="upload_script",
      action=dict(
        module='win_copy',
        args=dict(src='./scripts/powersploit/Privesc/PowerUp.ps1',dest='C:\PowerUp.ps1')
       ),
      register='upload_script',
      ),
     dict(name="upload_script debug",action=dict(module='debug', args=dict(var='upload_script')))
    ]

  return action


def unquoted_path_services():
  action = [
     dict(
      name="unquoted_path_services_ps_recon",
      action=dict(
        module='script',
        args='./scripts/infogather/unquoted_path_services.ps1'
      ),
      register='unquoted_path_services_ps_recon',
      ignore_errors='true'),
     #dict(name="extra_ps debug",action=dict(module='debug', args=dict(var='extra_ps')))
    ]
  return action


def unquoted_path_services_unprivileged():
  action = [
     dict(
      name="unquoted_path_services_unprivileged_ps_recon",
      action=dict(
        module='win_shell',
        args='wmic service get name,displayname,pathname,startmode |findstr /i "auto" |findstr /i /v "c:\windows"'
      ),
      register='unquoted_path_services_unprivileged_ps_recon',
      ignore_errors='true'),
    ]
  return action


def find_path_DLLHijack():
  action = [
     dict(
      name="find_path_DLLHijack_ps_recon",
      action=dict(
        module='script',
        args='./scripts/infogather/find_pathDLLHijack.ps1'
      ),
      register='find_path_DLLHijack_ps_recon',
      ignore_errors='true'),
     #dict(name="extra_ps debug",action=dict(module='debug', args=dict(var='extra_ps')))
    ]

  return action

def find_Get_ModifiableService():
  action = [
     dict(
      name="get_modifiable_service_ps_recon",
      action=dict(
        module='script',
        args='./scripts/infogather/get_modifiableService.ps1'
      ),
      register='get_modifiable_service_ps_recon',
      ignore_errors='true'),

    ]

  return action

def get_ModifiableScheduledTaskService():
  action = [
     dict(
      name="get_modifiable_scheduled_task_service_ps_recon",
      action=dict(
        module='script',
        args='./scripts/infogather/get_modifiableScheduledTaskFile.ps1'
      ),
      register='get_modifiable_scheduled_task_service_ps_recon',
      ignore_errors='true'),
    ]
  return action

def get_ModifiableRegistryAutorun():
  action = [
     dict(
      name="get_modifiable_registry_autorun_ps_recon",
      action=dict(
        module='script',
        args='./scripts/infogather/get_modifiableRegistryAutorun.ps1'
      ),
      register='get_modifiable_registry_autorun_ps_recon',
      ignore_errors='true'),
    ]
  return action

def generateChecksPlaybook():

  pp = pprint.PrettyPrinter(indent=2)

  tasks = []

  # debug connectivity
  # tasks.extend(win_ping())

  # install a service
  # tasks.extend(win_folder())
  # tasks.extend(win_copy())
  # tasks.extend(win_service())

  # ps tests and debug
  # tasks.extend(hello_ps())
  # tasks.extend(error_ps())
  # tasks.extend(error2_ps())
  # tasks.extend(extra_ps())

  # ps infogather scripts upload and run
  tasks.extend(upload_scripts())
  tasks.extend(unquoted_path_services())
  tasks.extend(unquoted_path_services_unprivileged())
  tasks.extend(find_path_DLLHijack())
  #tasks.extend(find_Get_ModifiableService())
  #tasks.extend(get_ModifiableScheduledTaskService())
  tasks.extend(get_ModifiableRegistryAutorun())

  return tasks





# Playbook generation as a folder structure -----------------------------------

