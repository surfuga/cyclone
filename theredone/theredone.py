# -*- coding: utf-8 -*-
from __future__ import (absolute_import, division, print_function)

import json
from collections import namedtuple
import subprocess
import boto3
import yaml
import pyaml
import pprint
import os
import sys
import argparse

from playbookRunner import *
from checksPlaybookGenerator import *
from attacksPlaybookGenerator import *

# functionalities ----------------------------------------------------------



# Main functions -------------------------------------------------------------

def run(region):

  # get info from hosts and group_vars/windows files

  # create the playbook (privesc_checks)
  privesc_checks=generateChecksPlaybook()

  # print(" infogathering (checks) action")
  # pp = pprint.PrettyPrinter(indent=2)
  # pp.pprint(privesc_checks)
  # print("")


  # generate a new playbook (privesc_actions)
  privesc_actions=generateAttacksPlaybook()

  privesc = privesc_checks + privesc_actions


  # launch it
  facts = provisionPlaybook(myplaybook=privesc, hosts='windows', hosts_filepath='../playbook/hosts', host_vars_filepath='../playbook/group_vars/windows')

  print(" execution result")
  pp = pprint.PrettyPrinter(indent=2)
  pp.pprint(facts.results[0]['tasks'])
  print("")





if __name__ == '__main__':

  region = 'eu-west-1'

  run(region)
