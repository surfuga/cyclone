# -*- coding: utf-8 -*-
from __future__ import (absolute_import, division, print_function)

import json
from collections import namedtuple
import subprocess
import boto3
import yaml
import pyaml
import pprint

import os
import sys
from collections import OrderedDict


import ansible
from ansible.parsing.dataloader import DataLoader
from ansible.vars import VariableManager
from ansible.inventory import Inventory
from ansible.inventory import group
from ansible.playbook.play import Play
from ansible.executor.task_queue_manager import TaskQueueManager
from ansible.plugins.callback.json import CallbackModule
from ansible.plugins.callback import CallbackBase






# Playbook generation as a dict -------------------------------------------

def win_ping_attack():
  action = [
     dict(
      name="win_ping_attack",
      action=dict(
        module='win_ping',
      ),
      when=u'win_ping|succeeded'
     ),

    ]

  return action


def hello_ps_attack():
  action = [
     dict(
      name="hello_ps_attack",
      action=dict(
        module='win_ping',
      ),
      when=u'hello_ps|succeeded'
     ),
    ]

  return action

def error_ps_attack():
  action = [
     dict(
      name="error_ps_attack",
      action=dict(
        module='win_ping',
      ),
      when=u"error_ps|succeeded"
     ),
    ]

  return action

def error2_ps_attack():
  action = [
     dict(
      name="error2_ps_attack",
      action=dict(
        module='win_ping',
      ),
      when=u"error2_ps|succeeded" #works
      #when="error2_ps.rc != 1" #not working
      #when=False #worked!
      #when=u"False"  #works
     ),
    ]

  return action


def unquoted_path_services_ps_attack():
  action = [
     dict(
      name="unquoted_path_services_ps_attack",
      action=dict(
        module='script',
        args='./scripts/privesc/unquoted_path_services.ps1'
      ),
      when=u"unquoted_path_services_ps_recon|succeeded",
      ignore_errors='true'),
    ]

  return action

def generateAttacksPlaybook():

  pp = pprint.PrettyPrinter(indent=2)

  tasks = []

  # tasks.extend(win_ping_attack())
  # tasks.extend(hello_ps_attack())
  # tasks.extend(error_ps_attack())
  # tasks.extend(error2_ps_attack())


  return tasks





# Playbook generation as a folder structure -----------------------------------

