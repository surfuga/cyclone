. C:\PowerUp.ps1
$returnvalue = Get-ServiceUnquoted
#{ $returnvalue.count } # outputs the string $returnvalue.count
#$returnvalue.count  # outputs a number

If ( $returnvalue.count -gt 0)
{
  $returnvalue | ForEach-Object {$_.Path}
  exit 0
}
Else
{
  exit 1
}
