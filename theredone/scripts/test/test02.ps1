. C:\PowerUp.ps1
$returnvalue = Get-ServiceUnquoted
Write-Host $returnvalue
Write-Host $returnvalue.count

If ( $returnvalue.count -gt 0 )
{
    Write-Host "Inside returnvalue.count > 0"
    $returnvalue | ForEach-Object {
      Write-Host "ServiceName:" $_.ServiceName
      Write-Host "Path:" $_.Path " ModifiablePath:" $_.ModifiablePath " AbuseFunction:" $_.AbuseFunction " CanRestart:" $_.CanRestart
    }
}
